Feature: Pull albums
  We need a way to get the photo albums

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/albums" endpoint

  Scenario: The user pulls all the photo albums
    When The user pulls the photo albums
    And The user makes the request
    Then The user should get all the photo albums
    And The user should get a http response code of 200

  Scenario: The user pulls all the photo albums of a user
    Given The user wants the albums of user #"4"
    When The user makes the request
    Then The user should get all the albums of user #"4"
    And The user should get a http response code of 200

  Scenario: The user pulls a specific album
    Given The user wants the photo album #"5"
    When The user makes the request
    Then The user should get the photo album #"5"
    And The user should get a http response code of 200

  Scenario Outline: The user pulls all the albums of a user with an invalid id
    Given The user wants the albums of user #"<userId>"
    When The user makes the request
    And The user should get a http response code of 400
    Examples:
      | userId   |
      | -4       |
      | wes841ls |
      |          |

  Scenario Outline: The user pulls an album with an invalid id
    Given The user wants the photo album #"<albumId>"
    When The user makes the request
    And The user should get a http response code of 400
    Examples:
      | albumId  |
      | -4       |
      | wes841ls |

  Scenario: The user pulls all the post from a non existing user
    Given The user wants the albums of user #"483773920"
    When The user makes the request
    And The user should get a http response code of 404

  Scenario: The user pulls a non existing album
    Given The user wants the photo album #"638810375692"
    When The user makes the request
    And The user should get a http response code of 404