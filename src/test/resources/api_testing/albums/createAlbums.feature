Feature: Create albums
  We need a way to let the user create new albums

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/albums" endpoint

  Scenario: The user creates a new photo album
    Given The user wants to create a new photo album
    When The user makes the request
    Then The user should successfully create a new photo album
    And The user should get a http response code of 201

  Scenario: The user creates a photo album with a blank field
    Given The user wants to create a new photo album with a blank "title"
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario Outline: The creates a photo album without a field
    Given The user wants to create a new photo album without a "<field>"
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field  |
      | title  |
      | userId |

  Scenario: The user creates a photo with an extra field
    Given The user wants to create a new photo album with an extra "major_lazer"
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user creates a photo album with a malformed JSON
    Given The user wants to create a new photo album
    When The user tries to create a new photo album with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user creates a photo album with an empty JSON
    When The user tries to create a new photo album
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user creates a photo album with an empty body
    When The user tries to create a photo album without a body
    When The user makes the request
    Then The user should get a http response code of 400