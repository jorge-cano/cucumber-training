Feature: Modify albums
  We need to have a way to modify the existing album info

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/albums" endpoint

  Scenario: The user changes the album info
    Given The user wants to change album #"4" with the following values:
      | userId | 8                                                             |
      | title  | vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? |
    When The user makes the request
    Then The user should be successful in changing the album info
    And The user should get a http response code of 200

  Scenario: The user changes the album info and adds a field
    Given The user wants to change album #"4" with the following values:
      | userId      | 8                                                             |
      | title       | vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? |
      | major_lazer | lazers never die                                              |
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario Outline: The user changes the info of an album with an invalid id
    Given The user wants to change album #"<albumId>" with the following values:
      | userId      | 8                                                             |
      | title       | vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? |
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | albumId |
      | -3      |
      | jhon30  |
      |         |

  Scenario: The user wants to change the info of a non existing album
    Given The user wants to change album #"478820970028" with the following values:
      | userId      | 8                                                             |
      | title       | vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? |
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario: The user changes the info of an album with an empty JSON
    Given The user wants to change album #"4" with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user changes the info of an album with a malformed JSON
    Given The user wants to change album #"4" with the following values:
      | userId      | 8                                                             |
      | title       | vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? |
    And The user tries to change the album info with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user changes the info of an album with an empty body
    When The user tries to change the album info with an empty body
    And The user makes the request
    Then The user should get a http response code of 400