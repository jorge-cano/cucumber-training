Feature: Modify Posts
  We need to be able to amend posts in our blog

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/posts" endpoint

  Scenario: User wants to edit a post's title
    Given That the user wants to change "title" of post #4
    When The user makes the request
    Then The changes to the post should be successful
    And The user should get a http response code of 200

  Scenario: User wants to edit a post's body
    Given That the user wants to change "body" of post #4
    When The user makes the request
    Then The changes to the post should be successful
    And The user should get a http response code of 200

  Scenario: User wants to edit a post's title to blank
    Given That the user wants to change "title" of post #"4" to blank
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: User wants to edit a post that does not exist
    Given That the user wants to change "title" of post #4456098
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario: User wants to edit an invalid field of a post
    Given That the user wants to change "major_lazer" of post #4
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario Outline: User wants to edit a field of a post with an invalid id
    Given That the user wants to change "title" of post #<postId>
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | postId |
      |-3      |
      |        |
      |4A-GE   |

  Scenario: User wants to edit a post but sends an malformed JSON
    Given That the user wants to change "body" of post #4
    And The user tries to edit the post with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400
    
  Scenario: User wants to edit a post but sends an empty body
    Given The user tries to edit post #4 with an empty body
    When The user makes the request
    Then The user should get a http response code of 400
    
  Scenario: User wants to edit a post but send an empty JSON  
    Given That the user wants to edit post #4 with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400