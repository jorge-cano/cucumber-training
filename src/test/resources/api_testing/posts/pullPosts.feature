Feature: Request Posts
  We need to retrieve posts from a blog

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/posts" endpoint

  Scenario Outline: User pulls the n'th post
    Given The user asks for the #<post_number> post
    When The user makes the request
    Then The user should be served the #<post_number> post
    And The user should get a http response code of 200

    Examples:
    |post_number|
    |1          |
    |2          |
    |3          |
    |4          |
    |5          |

  Scenario Outline: User pulls a post with an invalid post number
    Given The user asks for the #<post_number> post
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | post_number |
      |-4           |
      |             |
      |t1tdf2       |

  Scenario: User pulls a list of all the posts
    Given The user asks for all the posts
    When The user makes the request
    Then The user should be served a list of posts
    And The user should get a http response code of 200

  Scenario: User pulls a list of all the posts of another user
    Given The user asks for all of the posts of user #"3"
    When The user makes the request
    Then The user should be served all the posts of user #3
    And The user should get a http response code of 200

  Scenario: User tries to pull posts of a user that does not exist
    Given The user asks for all of the posts of user #"150000"
    When The user makes the request
    Then The user should get an empty list of posts
    And The user should get a http response code of 200

  Scenario Outline: User tries to pull posts of a user with an invalid value
    Given The user asks for all of the posts of user #"<userId>"
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | userId |
      |-1      |
      |        |
      |brn41   |