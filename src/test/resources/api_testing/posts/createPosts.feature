Feature: Create Posts
  We need to create new posts in our blog

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/posts" endpoint

  Scenario: User wants to post something
    Given That the user wants to post something new
    When The user makes the request
    Then A new post should be created successfully
    And The user should get a http response code of 201

  Scenario Outline: User wants to post with a blank title field
    Given That the user wants to post something new with a blank <field> field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field |
      |email  |
      |body   |
      |userId |

  Scenario Outline: User wants to post something without a field
    Given That the user wants to post something new without a <field> field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field |
      |email  |
      |body   |
      |userId |

  Scenario: User wants to post something with an extra field
    Given That the user wants to post something new with an extra major_lazer field
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: User wants to post something but has a malformed JSON
    Given The user tries to post something with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: User wants to post something with an empty request body
    Given The user tries to post something with an empty body
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: User wants to post something with an empty JSON
    Given That the user wants to post something with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400