Feature: Create photos
  We need a way for the user to upload photos to the platform

  Background: 
    Given The user is hitting the "https://jsonplaceholder.typicode.com/photos" endpoint
    
  Scenario: The user creates a new photo
    Given The user wants to upload a photo
    When The user makes the request
    Then The photo should be created
    And The user should get a http response code of 201

  Scenario Outline: The user creates a new photo with a blank field
    Given The user wants to upload a photo with a blank "<field>" field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field       |
      |title        |
      |url          |
      |thumbnailUrl |

  Scenario Outline: The user creates a new photo with a missing field
    Given The user wants to upload a photo with a missing "<field>" field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field       |
      |title        |
      |url          |
      |thumbnailUrl |

  Scenario: The user creates a photo with an extra field
    Given The user wants to upload a photo with an extra "major_lazer" field
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user creates a photo with a malformed JSON
    Given The user wants to upload a photo
    And The user tries to upload a photo with an invalid JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user creates a photo with an empty JSON
    Given The user wants to upload an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user creates a photo with an empty body
    Given The user tries to send an empty body
    When The user makes the request
    Then The user should get a http response code of 400