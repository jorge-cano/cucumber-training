Feature: Modify photos
  We need a way to modify the info of existing photos

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/photos" endpoint

  Scenario Outline: The user tries to change a field of a photo
    Given The user wants to change the "<field>" to "<value>" in photo #4
    When The user makes the request
    Then The photo should be updated
    And The user should get a http response code of 200
    Examples:
      | field        | value                                                                     |
      | url          | https://images.justwatch.com/backdrop/9348746/s1440/initial-d-first-stage |
      | thumbnailUrl | https://cde.peru.com//ima/0/1/2/0/9/1209692/611x458/anime.jpg             |
      | title        | vitae efficitur metus feugiat quis.                                       |

  Scenario: The user tries to update a non existing field on a photo
    Given The user wants to change the "major_lazer" to "sound" in photo #4
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario Outline: The user tries to update a field of a photo to blank
    Given The user wants to change the "<field>" to "" in photo #4
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field        |
      | url          |
      | thumbnailUrl |
      | title        |

  Scenario: The user tries to update a photo with a malformed JSON
    Given The user wants to change the "title" to "feugiat" in photo #3
    And The user tries to update the photo with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user tries to update a photo with an empty JSON
    Given The user wants to update photo #3 with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user tries to update a photo with an empty body
    Given The user tries to update photo #3 with an empty body
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user tries to update a field in a non existing photo
    Given The user wants to change the "title" to "Jejoncio" in photo #488917000
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario Outline: The user tries to update a photo with an invalid id
    Given The user wants to change the "title" to "Jejoncio" in photo #<photoId>
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | photoId |
      |-4       |
      |4tfe1    |
      |         |