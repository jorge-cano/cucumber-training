Feature: Pull Photos
  We need a way to get the photos

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/photos" endpoint

  Scenario: The user gets a photo
    Given The user wants to get the #3 photo
    When The user makes the request
    Then The user should get the #3 photo
    And The user should get a http response code of 200

  Scenario: The user gets all the photos from an album
    Given The user wants to get all the photos from the #4 album
    When The user makes the request
    Then The user should get all the photos from the #4 album
    And The user should get a http response code of 200

  Scenario: The user wants to get a non existing photo
    Given The user wants to get the #7729100 photo
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario: The user wants to get the photos of a non existing album
    Given The user wants to get all the photos from the #8801834 album
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario Outline: The user wants to get a photo with an invalid id
    Given The user wants to get the #<photoId> photo
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | photoId |
      |-3       |
      |5yh      |

  Scenario Outline: The user wants to get all the photos of an album with an invalid id
    Given The user wants to get all the photos from the #<albumId> album
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | albumId |
      |-3       |
      |5yh      |
      |         |
