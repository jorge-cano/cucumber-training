Feature: Modify comments
  We need a way to amend comments

  Background: 
    Given The user is hitting the "https://jsonplaceholder.typicode.com/comments" endpoint

  Scenario Outline: The user changes a field of a comment
    Given The user wants to change the <field> of comment #"6"
    When The user makes the request
    Then The change should take place
    And The user should get a http response code of 200
    Examples:
      | field |
      |name   |
      |body   |

  Scenario: The user tries to update a non existing field on a comment
    Given The user wants to change the major_lazer of comment #"6"
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario Outline: The user tries to change a field of a comment to blank
    Given The user wants to change the <field> of comment #6 to blank
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field |
      |name   |
      |body   |
      |email  |

  Scenario: The user tries to change a comment with an empty JSON
    Given The user wants to change comment #6 with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user tries to change a comment with a malformed JSON
    Given The user wants to change the body of comment #"6"
    And The user tries to change the comment with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400
    
  Scenario: The user tries to change a comment with an empty body
    Given The user wants to change the body of comment #"6"
    And The user tries to change the comment with and empty body
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user tries to edit a non existing comment
    Given The user wants to change the body of comment #"460000"
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario Outline: The user tries to edit a comment with an invalid id
    Given The user wants to change the body of comment #"<commentId>"
    When The user makes the request
    Then The user should get a http response code of <errorCode>
    Examples:
      | commentId | errorCode |
      |-3         |400        |
      |4rf        |400        |
      |           |400        |