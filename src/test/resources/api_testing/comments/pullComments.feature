Feature: Get Comments
  We need a way to get the comments of the posts

  Background: 
    Given The user is hitting the "https://jsonplaceholder.typicode.com/comments" endpoint
  
  Scenario: The user gets the comments on a post
    Given The user wants to see the comments on post #8
    When The user makes the request
    Then The user should get the comments of post #8
    And The user should get a http response code of 200

  Scenario: The user wants to get all the comments of a user
    Given The user wants to see all the comments of the user with email "Nikita@garfield.biz"
    When The user makes the request
    Then The user should get the comments of the user with email "Nikita@garfield.biz"
    And The user should get a http response code of 200

  Scenario Outline: The user wants to get all the comments of a invalid post
    Given The user wants to see the comments on post #<postId>
    When The user makes the request
    Then The user should get a http response code of <errorCode>
    Examples:
      | postId | errorCode |
      |        |400        |
      |-3      |400        |
      |4rt67   |400        |

  Scenario: The user wants to get all the comments of a non existing post
    Given The user wants to see the comments on post #3450800
    When The user makes the request
    Then The user should get a http response code of 404

