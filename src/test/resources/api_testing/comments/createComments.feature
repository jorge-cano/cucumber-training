Feature: Create comments
  We need a way for the users to leave comments on each other posts

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/comments" endpoint

  Scenario: The user creates a new comment on a post
    Given The user wants to create a new comment on post #6
    When The user makes the request
    Then The user should be successful in posting a comment
    And The user should get a http response code of 201

  Scenario Outline: The user creates a new comment with a blank field
    Given The user wants to create a new comment on post #6 with a blank <field>
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field |
      |name   |
      |email  |
      |body   |

  Scenario Outline: The user creates a new comment with a missing field
    Given The user wants to create a new comment on post #6 without an <field>
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field |
      |name   |
      |email  |
      |body   |

  Scenario: The user creates a new comment with an extra field
    Given The user wants to create a new comment on post #6 with an extra "major_lazer" field
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to create a new comment with an empty JSON
    Given The user wants to create a new comment on post #6 with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to create a new comment with a malformed JSON
    Given The user wants to create a new comment on post #6
    And The user wants to post a comment with malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to create a new comment with an empty body
    Given The user wants to post a comment with an empty body
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario Outline: The user wants to create a new comment with an invalid email
    Given The user wants to create a new comment on post #6 with an invalid email: "<email>"
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | email         |
      |foo            |
      |foo@           |
      |foo@var        |
      |foo-s@var.com  |