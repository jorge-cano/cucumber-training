Feature: Update to-dos
  We need a way to update to-dos

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/todos" endpoint

  Scenario: The user wants to mark a todo as done
    Given The user wants to mark todo #6 as done
    When The user makes the request
    Then The update should be successful
    And The user should get a http response code of 200

  Scenario: The user wants to change the title of a todo
    Given The user wants to change the "title" of todo #6
    When The user makes the request
    Then The update should be successful
    And The user should get a http response code of 200
    
  Scenario: The user changes the title of a todo to blank
    Given The user wants to change the "title" of todo #"6" to blank
    When The user makes the request
    Then The user should get a http response code of 400
    
  Scenario: The user tries to update a non existing field
    Given The user wants to change the "major_lazer" of todo #10
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user marks a todo as done with an invalid value
    Given The user wants mark todo #6 as done with an invalid value
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user updates a todo with an empty JSON
    Given The user wants to update todo #6 with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user updates a todo with an empty body
    Given The user wants to mark todo #7 as done
    When The user tries to update the todo with an empty body
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user updates a todo with a malformed JSON
    Given The user wants to mark todo #10 as done
    And The user tries to update the todo with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to mark a non existing todo as done
    Given The user wants to mark todo #1500 as done
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario Outline: The user updates an unspecified todo
    Given The user wants to mark todo #<todoId> as done
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | todoId |
      |        |
      |-3      |
      |4bl4    |
