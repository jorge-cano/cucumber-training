Feature: Get To-dos
  The user needs a way to query his to-dos

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/todos" endpoint

  Scenario: The user gets their to-dos
    Given The user #6 wants to get his to-dos
    When The user makes the request
    Then The user #6 should get a list with their to-dos
    And The user should get a http response code of 200

  Scenario: A non existing user tries to get his to-dos
    Given The user #2400 wants to get his to-dos
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario Outline: The user tries to get to-dos with invalid id
    Given The user #<userId> wants to get his to-dos
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | userId |
      |kh24.   |
      |        |
      |~%ysha  |
      |5t_ge   |