Feature: Create To-dos
  We need a way to create new To-dos

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/todos" endpoint

  Scenario: The user wants to create a new todo
    Given The user has a new to-do
    When The user makes the request
    Then The new todo should be created
    And The user should get a http response code of 201

  Scenario Outline: The user wants to create a todo with a blank field
    Given The user wants to create a todo with a blank <field>
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field     |
      |userId     |
      |title      |
      |completed  |

  Scenario Outline: The user wants to create a todo without a field
    Given The user wants to create a todo leaving out the <field>
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field     |
      |userId     |
      |title      |
      |completed  |

  Scenario: The user wants to create a todo with an extra field
    Given The user wants to create a todo with an extra "major_lazer" field
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to create a todo with an empty JSON
    Given The user wants to create a todo sending an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to create a todo but sends a malformed JSON
    Given The user tries to create a new todo with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to create a todo but sends an empty body
    Given The user tries to create a new todo with an empty body
    When The user makes the request
    Then The user should get a http response code of 400