Feature: Create Users
  We need a way to create new users in the system

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/users" endpoint

  Scenario: The user registers in the platform
    Given The user fills in his info for the platform
    When The user makes the request
    Then The user should be registered
    And The user should get a http response code of 201

  Scenario Outline: The user registers with a blank field
    Given The user fills in his info for the platform with a blank "<field>" field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field   |
      |name     |
      |username |
      |email    |
      |company  |
      |address  |

  Scenario Outline: The user registers with a missing field
    Given The user fills in his info for the platform without a "<field>" field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field   |
      |name     |
      |username |
      |email    |
      |company  |
      |address  |

  Scenario: The user registers with an extra field
    Given The user fills in his info for the platform with an extra "major_lazer" field
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user registers with a malformed JSON
    Given The user sends his info with a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user registers with an empty JSON
    Given The user wants to register with an empty JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user registers with an empty body
    Given The user sends no info
    When The user makes the request
    Then The user should get a http response code of 400