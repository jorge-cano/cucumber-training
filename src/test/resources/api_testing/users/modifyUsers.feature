Feature: Modify User Info
  We need to have a way to update user info

  Background:
    Given The user is hitting the "https://jsonplaceholder.typicode.com/users" endpoint

  Scenario Outline: The user changes his some info
    Given The user #<user_id> wants to change their "<field>"
    When The user makes the request
    Then The user should be successful in changing his "<field>"
    And The user should get a http response code of 200
    Examples:
      | user_id | field |
      |4        |email  |
      |9        |username|
      |3        |name    |

  Scenario: The user changes all of his info
    Given The user #4 wants to update all of his info
    When The user makes the request
    Then The user should be successful in changing his info

  Scenario: A non-registered user tries to change his info
    Given The user #240 wants to update all of his info
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario: A non-registered user tries to change some of his info
    Given The user #240 wants to change their "email"
    When The user makes the request
    Then The user should get a http response code of 404

  Scenario Outline: A user tries to change all of his info but leaves a field blank
    Given The user #4 wants to update all of his info with a blank "<field>" field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field   |
      |name     |
      |username |
      |email    |
      |company  |
      |address  |

  Scenario Outline: A user tries to change part of his info and leaves it blank
    Given The user #4 wants to change their "<field>" to blank
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field   |
      |name     |
      |username |
      |email    |
      |company  |
      |address  |

  Scenario Outline: A user tries to change all his info but leaves out a field
    Given The user #4 wants to update all of his info without a "<field>" field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field   |
      |name     |
      |username |
      |email    |
      |company  |
      |address  |

  Scenario Outline: A user tries to change his email but leaves out that field
    Given The user #4 wants to change their "<field>" without that field
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | field   |
      |name     |
      |username |
      |email    |
      |company  |
      |address  |

  Scenario: A user tries to change all his info with an extra major_lazer field
    Given The user #4 wants to update all of his info with an extra "major_lazer" field
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: A user tries to change a non existing "major_lazer" field
    Given The user #4 wants to change their "major_lazer"
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user changes all of his info but sends a malformed JSON
    Given The user #4 wants to update all of his info
    And The user tries to change all of his info but sends a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user changes some of his info but sends a malformed JSON
    Given The user #4 wants to change their "email"
    And The user tries to change part of his info but sends a malformed JSON
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to change all of his info but sends an empty body
    Given The user #4 wants to update all of his info
    And The user tries to change all of his info with an empty body
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario: The user wants to change some of his info but sends an empty body
    Given The user #4 wants to change their "email"
    And The user tries to change part of his info with an empty body
    When The user makes the request
    Then The user should get a http response code of 400

  Scenario Outline: An invalid user wants to change all their info
    Given The user #<userId> wants to update all of his info
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | userId |
      |        |
      |-3      |
      |$get56  |

  Scenario Outline: An invalid user wants to change their email
    Given The user #<userId> wants to change their "email"
    When The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | userId |
      |        |
      |-3      |
      |$get56  |