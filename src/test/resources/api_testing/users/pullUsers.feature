Feature: Request Users info
  We need to retrieve info of our users.

  Background: 
    Given The user is hitting the "https://jsonplaceholder.typicode.com/users" endpoint
    
  Scenario: The user retrieves information about an account.
    When The user retrieves info about the account with username "Kamren"
    And The user makes the request
    Then The user should receive all the info of the account with username "Kamren"
    And The user should get a http response code of 200

  Scenario: The user retrieves information about all accounts.
    When The user retrieves info about all accounts
    And The user makes the request
    Then The user should receive all the info of all the accounts
    And The user should get a http response code of 200

  Scenario Outline: The user retrieves information about an account with invalid values.
    When The user retrieves info about the account with username "<userName>"
    And The user makes the request
    Then The user should get a http response code of 400
    Examples:
      | userName |
      |          |
      |6Det_5*?  |
      |~Termin   |
      |don.mna   |

  Scenario: The user tries to retrieve information about an account that does not exist.
    When The user retrieves info about the account with username "UribecoSanchanzoMela"
    And The user makes the request
    Then The user should receive an empty list
    And The user should get a http response code of 200