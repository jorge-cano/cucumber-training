@RunThis
Feature:Kafka gender remapping
  We need a way to remap genders via stream processors in kafka


  Scenario Outline: I send an old style person record
    Given I have an old style person record with a "<old_gender_code>" gender
    When I send the record to the "streams-remap-input" topic
    Then I should get the record with a "<new_gender_code>" gender
    Examples:
      | old_gender_code | new_gender_code |
      | M               | MALE            |
      | F               | FEMALE          |
      | B               | OTHER           |
      | O               | OTHER           |

  Scenario: I send a person record without gender
    Given I have an old style person record with no gender field
    When I send the record to the "streams-remap-input" topic
    Then I should see the same record in the "streams-remap-genderless-output" topic

  Scenario: I send a garbage non-json compliant record
    When I send garbage data
    Then I should see that same data in the "streams-remap-invalid-output" topic

  Scenario: I send an old style record
    Given I have this old style record:
    """
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "gender": M,
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    }
    """
    When I send the record to the "streams-remap-input" topic
    Then I should see the following record in the "streams-remap-output" topic:
    """
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "gender": MALE,
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    }
    """

  Scenario: I send an old style record
    Given I have this old style record:
    """
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "gender": M,
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    }
    """
    When I send the record to the "streams-remap-input" topic
    Then I should see a record similar to this one in the "streams-remap-output" topic:
    """
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "gender": MALE,
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    }
    """