package api_testing.utils

import org.apache.commons.logging.Log
import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.LongDeserializer
import org.apache.kafka.common.serialization.StringDeserializer
import java.util.*
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.ConsumerRecord
import java.time.Duration
import java.util.logging.Level
import java.util.logging.Logger
import org.apache.kafka.common.TopicPartition




class KafkaRemapConsumer(private val topic: String) {

    private val bootstrapServer = "localhost:9092"

    private fun createConsumer(): Consumer<Long, String> {
        val props = Properties()

        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                this.bootstrapServer)
        props.put(ConsumerConfig.GROUP_ID_CONFIG,
                "KafkaTestConsumer")
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                LongDeserializer::class.java.name)
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer::class.java.name)

        // Create the consumer using props.
        val consumer = KafkaConsumer<Long, String>(props)

        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(this.topic))
        return consumer
    }

    fun consumeTopicRecord(): String {

        val consumer = createConsumer()

        //Move consumer offset to latest message offset in the assigned topics
        val assignedPartitions = consumer.assignment()
        consumer.seekToEnd(assignedPartitions)
        
        for (partition in assignedPartitions) {

            val offset = consumer.position(partition)
            consumer.seek(partition, offset)
        }

        var records = consumer.poll(Duration.ofMillis(1000))
        while(records.isEmpty){
            records = consumer.poll(Duration.ofMillis(1000))
        }

        val consumedValue = records.records(this.topic).last().value()
        Logger.getAnonymousLogger().log(Level.INFO, "Consumer value: $consumedValue")

        consumer.commitSync()
        consumer.close()

        return consumedValue

    }
}