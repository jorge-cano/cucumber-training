package api_testing.utils

import cucumber.api.java.en.When
import io.cucumber.pro.shaded.org.eclipse.jgit.transport.UploadPack
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder

class HttpRequest(private val responseValidator: ResponseCodeValidator) {

    var url: String? = null
    var requestMethod: String? = null
    var payload: String? = null
    private val client: CloseableHttpClient = HttpClientBuilder.create().build()
    var response: HttpResponse? = null
    var urlParameters: HashMap<String,String>? = null

    @When("^The user makes the request$")
    fun userMakesTheRequest() {

        when {
            this.payload != null -> this.response = this.client.execute(RequestSetupHandler.setupRequest(
                    this.url!!,
                    this.requestMethod!!,
                    this.payload!!
            ))

            this.urlParameters != null -> this.response = this.client.execute(RequestSetupHandler.setupRequest(
                    this.url!!,
                    this.requestMethod!!,
                    this.urlParameters!!
            ))

            else -> this.response = this.client.execute(RequestSetupHandler.setupRequest(
                    this.url!!,
                    this.requestMethod!!
            ))
        }

        this.responseValidator.setClient(this.client)
        this.responseValidator.setResponse(this.response!!)
    }
}