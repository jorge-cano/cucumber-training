package api_testing.utils

import org.apache.http.client.methods.*
import org.apache.http.client.utils.URIBuilder
import org.apache.http.entity.StringEntity

import java.io.IOException
import java.net.*

object RequestSetupHandler {

    @Throws(IOException::class)
    fun setupRequest(url: URI, requestMethod: String): HttpUriRequest? {

        return when (requestMethod) {

            "GET" -> HttpGet(url)

            "POST" -> HttpPost(url)

            "PATCH" -> HttpPatch(url)

            "PUT" -> HttpPut(url)

            else -> null
        }
    }

    @Throws(IOException::class, URISyntaxException::class)
    fun setupRequest(url: String, requestMethod: String): HttpUriRequest? {

        return setupRequest(URI(url), requestMethod)
    }

    @Throws(IOException::class, URISyntaxException::class)
    fun setupRequest(
            url: String,
            requestMethod: String,
            urlParameters: Map<String, String>
    ): HttpUriRequest? {

        return setupRequest(getURLWithParamsString(url, urlParameters), requestMethod)
    }


    @Throws(IOException::class)
    fun setupRequest(url: URI, requestMethod: String, jsonToSend: String): HttpUriRequest? {

        val request: HttpUriRequest?

        when (requestMethod) {

            "POST" -> {

                val postRequest = HttpPost(url)

                postRequest.setHeader("Content-Type", "application/json; charset=UTF-8")
                postRequest.setHeader("Accept", "application/json")

                postRequest.entity = StringEntity(jsonToSend)

                request = postRequest
            }

            "PATCH" -> {

                val patchRequest = HttpPatch(url)

                patchRequest.setHeader("Content-Type", "application/json; charset=UTF-8")
                patchRequest.setHeader("Accept", "application/json")

                patchRequest.entity = StringEntity(jsonToSend)

                request = patchRequest
            }

            "PUT" -> {

                val putRequest = HttpPut(url)

                putRequest.setHeader("Content-Type", "application/json; charset=UTF-8")
                putRequest.setHeader("Accept", "application/json")

                putRequest.entity = StringEntity(jsonToSend)

                request = putRequest
            }

            else -> request = null
        }

        return request
    }

    @Throws(IOException::class, URISyntaxException::class)
    fun setupRequest(
            url: String,
            requestMethod: String,
            jsonToSend: String
    ): HttpUriRequest? {

        return setupRequest(URI(url), requestMethod, jsonToSend)
    }

    @Throws(IOException::class, URISyntaxException::class)
    fun setupRequest(
            url: String,
            requestMethod: String,
            urlParameters: Map<String, String>,
            jsonToSend: String
    ): HttpUriRequest? {

        return setupRequest(getURLWithParamsString(url, urlParameters), requestMethod, jsonToSend)
    }

    @Throws(URISyntaxException::class)
    private fun getURLWithParamsString(url: String, params: Map<String, String>): URI {

        val builder = URIBuilder(url)

        for ((key, value) in params) {

            builder.addParameter(key, value)
        }

        return builder.build()
    }
}
