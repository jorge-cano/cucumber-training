package api_testing.utils

import org.apache.kafka.clients.producer.*
import org.apache.kafka.common.serialization.LongSerializer
import org.apache.kafka.common.serialization.StringSerializer
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger


class KafkaRemapProducer(private val topic: String) {

    private val bootstrapServer = "localhost:9092"

    private fun createProducer(): Producer<Long, String> {
        val props = Properties()

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                this.bootstrapServer)
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaTestingProducer")
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                LongSerializer::class.java.name)
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer::class.java.name)

        return KafkaProducer(props)
    }

    fun sendToTopic(message: String){

        val producer: Producer<Long, String> = this.createProducer()
        val time: Long = System.currentTimeMillis()
        val record: ProducerRecord<Long, String> = ProducerRecord(this.topic, time, message)

        val metadata: RecordMetadata = producer.send(record).get()
        System.out.printf("sent record(key=%s value=%s) " +
                "meta(partition=%d, offset=%d) time=%d\n",
                record.key(), record.value(), metadata.partition(),
                metadata.offset(), time)

        Logger.getAnonymousLogger().log(Level.INFO, "Producer value: $message")

        producer.flush()
        producer.close()
    }
}