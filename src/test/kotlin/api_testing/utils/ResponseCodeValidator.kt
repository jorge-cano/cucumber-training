package api_testing.utils

import cucumber.api.java.en.Then
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.junit.Assert

import java.io.IOException

class ResponseCodeValidator {

    private var response: HttpResponse? = null
    private var client: CloseableHttpClient? = null

    fun setResponse(response: HttpResponse) {
        this.response = response
    }

    fun setClient(client: CloseableHttpClient) {
        this.client = client
    }

    @Then("^The user should get a http response code of (\\d+)$")
    @Throws(IOException::class)
    fun checkResponseCode(responseCode: Int) {

        Assert.assertEquals(responseCode.toLong(), this.response!!.statusLine.statusCode.toLong())
        this.client!!.close()
    }
}
