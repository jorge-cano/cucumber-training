package api_testing.utils

import cucumber.api.java.en.Given

class EndpointAccess {

    var targetURL: String? = null
        private set

    @Given("The user is hitting the \"([^\"]*)\" endpoint")
    fun userTargetsEndpoint(targetURL: String) {

        this.targetURL = targetURL
    }

    fun addToPath(path: String) {

        this.targetURL = this.targetURL + "/" + path
    }
}
