package api_testing.utils

import org.json.JSONObject
import org.junit.Assert

object JSONSimilar {

    fun assertSimilar(expectedJSON: JSONObject, actualJSON: JSONObject) {

        for(key in expectedJSON.keys()){

            Assert.assertTrue(key in actualJSON.keySet())
        }
    }
}