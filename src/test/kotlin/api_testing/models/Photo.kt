package api_testing.models

import com.github.javafaker.Faker
import org.json.JSONArray
import org.json.JSONObject

class Photo {

    private val faker = Faker()

    fun create(): JSONObject {

        val photo = JSONObject()

        photo.put("albumId", this.faker.number().numberBetween(0, 100))
                .put("title", this.faker.lorem().sentence())
                .put("url", this.faker.internet().url())
                .put("thumbnailUrl", this.faker.internet().url())

        return photo
    }

    fun createWithBlankField(field: String): JSONObject {

        val photo: JSONObject = this.create()

        photo.put(field, "")

        return photo
    }

    fun createWithoutField(field: String): JSONObject {

        val photo: JSONObject = this.create()

        photo.remove(field)

        return photo
    }

    fun createWithExtraField(field: String): JSONObject {

        val photo: JSONObject = this.create()

        photo.put(field, this.faker.rickAndMorty().quote())

        return photo
    }
}
