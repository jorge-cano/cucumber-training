package api_testing.models

import com.github.javafaker.Faker
import org.json.JSONObject

class Post {

    private val faker = Faker()

    fun create(): JSONObject {

        val post = JSONObject()

        post.put("userId", faker.number().randomDigit())
        post.put("title", faker.lorem().characters(6, 11))
        post.put("body", faker.lorem().characters(50, 100))

        return post
    }

    fun createWithBlankField(field: String): JSONObject {

        val post: JSONObject = this.create()

        post.put(field, "")

        return post
    }

    fun createWithMissingField(field: String): JSONObject {

        val post: JSONObject = this.create()

        post.remove(field)

        return post
    }

    fun createWithExtraField(field: String): JSONObject {

        val post: JSONObject = this.create()

        post.put(field, this.faker.rickAndMorty().quote())

        return post
    }

    fun updateField(field: String): JSONObject {

        val post = JSONObject()

        post.put(field, this.faker.lorem().sentence())

        return post
    }

    fun updateFieldToBlank(field: String): JSONObject {

        val post = JSONObject()

        post.put(field, "")

        return post
    }
}
