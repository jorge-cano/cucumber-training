package api_testing.models

import com.github.javafaker.Faker
import org.json.JSONObject

class Todo {

    private val faker = Faker()

    fun create(): JSONObject {

        val todo = JSONObject()

        todo.put("userId", this.faker.number().randomDigit())
                .put("title", this.faker.lorem().sentence())
                .put("completed", false)

        return todo
    }

    fun createWithBlankField(field: String): JSONObject {

        val todo: JSONObject = this.create()

        todo.put(field, "")

        return todo
    }

    fun createWithoutField(field: String): JSONObject {

        val todo: JSONObject = this.create()

        todo.remove(field)

        return todo
    }

    fun createWithExtraField(field: String): JSONObject {

        val todo: JSONObject = this.create()

        todo.put(field, this.faker.rickAndMorty().quote())

        return todo
    }

    fun updateField(field: String): JSONObject {

        val todo = JSONObject()

        todo.put(field, this.faker.lorem().sentence())

        return todo
    }

    fun updateFieldToBlank(field: String): JSONObject {

        val todo = JSONObject()

        todo.put(field, "")

        return todo
    }

    fun markAs(done: Boolean): JSONObject {

        val todo = JSONObject()

        todo.put("completed", done)

        return todo
    }
}
