package api_testing.models

import com.github.javafaker.Faker
import org.json.JSONObject

class User {

    private val faker = Faker()

    fun create(): JSONObject {

        val user = JSONObject()

        user.put("name", this.faker.name().fullName())
        user.put("username", this.faker.name().username())
        user.put("email", this.faker.internet().emailAddress())
        user.put("phone", this.faker.phoneNumber().cellPhone())
        user.put("website", this.faker.internet().domainName() + "." + this.faker.internet().domainSuffix())
        user.put("address", this.createAddress())
        user.put("company", this.createCompany())

        return user
    }

    private fun createAddress(): JSONObject {

        val address = JSONObject()

        address.put("street", this.faker.address().streetName())
        address.put("suite", this.faker.number().digits(3))
        address.put("city", this.faker.address().cityName())
        address.put("zipcode", this.faker.address().zipCode())
        address.put("geo", createGeo())

        return address
    }

    private fun createCompany(): JSONObject {

        val company = JSONObject()

        company.put("name", this.faker.name().name())
        company.put("catchPhrase", this.faker.rickAndMorty().quote())
        company.put("bs", this.faker.rickAndMorty().quote())

        return company
    }

    private fun createGeo(): JSONObject {

        val geo = JSONObject()

        geo.put("lat", this.faker.address().latitude())
        geo.put("lng", this.faker.address().longitude())

        return geo
    }

    fun createWithBlankField(field: String): JSONObject {

        val user: JSONObject = this.create()

        user.put(field, "")

        return user
    }

    fun createWithoutField(field: String): JSONObject {

        val user: JSONObject = this.create()

        user.remove(field)

        return user
    }

    fun createWithExtraField(field: String): JSONObject {

        val user: JSONObject = this.create()

        user.put(field, this.faker.rickAndMorty().quote())

        return user
    }

    fun updateField(field: String): JSONObject {

        val user: JSONObject = JSONObject()

        user.put(field, this.faker.lorem().sentence())

        return user
    }

    fun createWithGender(gender: String): JSONObject {

        val user: JSONObject = this.create()

        user.put("gender", gender)

        return user
    }
}
