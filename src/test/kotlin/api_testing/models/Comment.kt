package api_testing.models

import com.github.javafaker.Faker
import org.json.JSONObject

class Comment {

    private val faker = Faker()

    fun create(postId: Int): JSONObject {

        val comment = JSONObject()

        comment.put("postId", postId)
                .put("name", this.faker.rickAndMorty().location())
                .put("email", this.faker.internet().emailAddress())
                .put("body", this.faker.lorem().paragraph())

        return comment
    }

    fun createWithBlankField(field: String, postId: Int): JSONObject {

        val comment: JSONObject = this.create(postId)

        comment.put(field, "")

        return comment
    }

    fun createWithMissingField(field: String, postId: Int): JSONObject {

        val comment: JSONObject = this.create(postId)

        comment.remove(field)

        return comment
    }

    fun createWithExtraField(field: String, postId: Int): JSONObject {

        val comment: JSONObject = this.create(postId)

        comment.put(field, this.faker.rickAndMorty().quote())

        return comment
    }

    fun createWithInvalidEmail(postId: Int, email: String): JSONObject {

        val comment: JSONObject = this.create(postId)

        comment.put("email", email)

        return comment
    }

    fun updateField(field: String): JSONObject {

        val comment = JSONObject()

        comment.put(field, this.faker.lorem().sentence())

        return comment
    }
}
