package api_testing.models

import com.github.javafaker.Faker
import org.json.JSONObject

class Album {

    private val faker = Faker()

    fun create(): JSONObject{

        val album = JSONObject()

        album.put("userId", this.faker.number().randomDigitNotZero())
                .put("title", this.faker.lorem().sentence())

        return album
    }

    fun createWithBlankField(field: String): JSONObject{

        val album: JSONObject = this.create()

        album.put(field, "")

        return album
    }

    fun createWithoutField(field: String): JSONObject{

        val album: JSONObject = this.create()

        album.remove(field)

        return album
    }

    fun createWithExtraField(field: String): JSONObject{

        val album: JSONObject = this.create()

        album.put(field, this.faker.rickAndMorty().quote())

        return album
    }

    fun createWithValues(values: Map<*, *>): JSONObject {

        val album = JSONObject()

        for((key, value) in values){

            album.put(key as String, value)
        }

        return album
    }


}