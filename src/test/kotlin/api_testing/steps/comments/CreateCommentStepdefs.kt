package api_testing.steps.comments

import api_testing.models.Comment
import api_testing.models.Post
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.net.URISyntaxException

class CreateCommentStepdefs(
        private val commentCreator: Comment,
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var comment: JSONObject? = null

    @Given("^The user wants to create a new comment on post #(\\d+)$")
    fun userWantsToCreateComment(postId: Int) {

        this.comment = this.commentCreator.create(postId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }

    @Then("^The user should be successful in posting a comment$")
    @Throws(IOException::class)
    fun userShouldBeSuccessful() {

        val createdComment = JSONObject(JSONTokener(this.request.response!!.entity.content))

        this.comment?.put("id", createdComment.get("id"))

        JSONAssert.assertEquals(this.comment, createdComment, false)
    }

    @Given("^The user wants to create a new comment on post #(\\d+) with a blank ([^\"]*)$")
    fun userWantsToCreateCommentWithBlankField(postId: Int, field: String) {

        this.comment = this.commentCreator.createWithBlankField(field, postId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }

    @Given("^The user wants to create a new comment on post #(\\d+) without an ([^\"]*)$")
    fun userWantsToCreateCommentWithoutField(postId: Int, field: String) {

        this.comment = this.commentCreator.createWithMissingField(field, postId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }

    @Given("^The user wants to create a new comment on post #(\\d+) with an extra \"([^\"]*)\" field$")
    fun userWantsToCreateNewCommentWithExtraField(postId: Int, field: String) {

        this.comment = this.commentCreator.createWithExtraField(field, postId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }

    @Given("^The user wants to create a new comment on post #6 with an empty JSON$")
    fun userWantsToCreateCommentWithEmptyJson() {

        this.comment = JSONObject()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }

    @When("^The user wants to post a comment with malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userWantsToPostCommentWithMalformedJson() {

        val jsonComment: String = this.comment.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = jsonComment.substring(0, jsonComment.length - 2)
    }

    @When("^The user wants to post a comment with an empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userWantsToPostCommentWithEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
    }

    @Given("^The user wants to create a new comment on post #(\\d+) with an invalid email: \"([^\"]*)\"$")
    fun userWantsToCreateCommentWithInvalidEmail(postId: Int, email: String) {

        this.commentCreator.createWithInvalidEmail(postId, email)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }
}
