package api_testing.steps.comments

import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

import java.io.IOException
import java.net.URISyntaxException
import java.util.HashMap

class PullCommentsStepdefs(
        private val endpoint: EndpointAccess,
        private val urlParameters: HashMap<String, String>,
        private val request: HttpRequest
) {

    @Given("^The user wants to see the comments on post #([^\"]*)$")
    fun userWantsToSeeCommentsOnPost(postId: String) {

        this.urlParameters["postId"] = postId

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
        this.request.urlParameters = this.urlParameters
    }

    @Then("^The user should get the comments of post #(\\d+)$")
    @Throws(IOException::class)
    fun userShouldGetCommentsOfPost(postId: Int) {

        Assert.assertEquals(200, this.request.response!!.statusLine.statusCode.toLong())

        val comments = JSONArray(JSONTokener(this.request.response!!.entity.content))

        Assert.assertTrue(comments.length() > 0)
        Assert.assertEquals(postId, comments.getJSONObject(0).get("postId"))
    }

    @Given("^The user wants to see all the comments of the user with email \"([^\"]*)\"$")
    fun userWantsToSeeCommentsOfUser(email: String) {

        this.urlParameters["email"] = email

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
        this.request.urlParameters = this.urlParameters
    }

    @Then("^The user should get the comments of the user with email \"([^\"]*)\"$")
    @Throws(IOException::class)
    fun userShouldGetTheCommentsOfUser(email: String) {

        Assert.assertEquals(200, this.request.response!!.statusLine.statusCode.toLong())

        val comments = JSONArray(JSONTokener(this.request.response!!.entity.content))

        Assert.assertTrue(comments.length() > 0)
        Assert.assertEquals(email, comments.getJSONObject(0).get("email"))
    }

}
