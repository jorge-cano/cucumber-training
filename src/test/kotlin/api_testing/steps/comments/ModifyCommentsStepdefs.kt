package api_testing.steps.comments

import api_testing.models.Comment
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import io.cucumber.pro.shaded.com.sun.jna.platform.win32.SetupApi
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.net.URISyntaxException

class ModifyCommentsStepdefs(
        private val commentCreator: Comment,
        private val request: HttpRequest,
        private val endpoint: EndpointAccess
) {

    private var comment: JSONObject? = null

    @Given("^The user wants to change the ([^\"]*) of comment #\"([^\"]*)\"$")
    fun userWantsToChangeFieldOfComment(field: String, commentId: String) {

        this.endpoint.addToPath(commentId)
        this.comment = this.commentCreator.updateField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.comment.toString()
    }

    @Then("^The change should take place$")
    @Throws(IOException::class)
    fun changesShouldTakePlace() {

        val updatedComment = JSONObject(JSONTokener(this.request.response!!.entity.content))

        for (key in this.comment!!.keySet()) {

            Assert.assertEquals(updatedComment.get(key), this.comment!!.get(key))
        }
    }

    @Given("^The user wants to change the ([^\"]*) of comment #(\\d+) to blank$")
    fun userWantsToChangeFieldToBlank(field: String, commentId: Int) {

        this.comment = JSONObject().put(field, "")
        this.endpoint.addToPath(commentId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }

    @Given("^The user wants to change comment #(\\d+) with an empty JSON$")
    fun userWantsToChangeCommentWithEmptyJson(commentId: Int) {

        this.comment = JSONObject()
        this.endpoint.addToPath(commentId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.comment.toString()
    }

    @When("^The user tries to change the comment with a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToChangeCommentWithMalformedJson() {

        val jsonComment: String = this.comment!!.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = jsonComment.substring(0, jsonComment.length - 2)
    }

    @When("^The user tries to change the comment with and empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToChangeCommentWithEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
    }
}
