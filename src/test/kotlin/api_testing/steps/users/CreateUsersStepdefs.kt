package api_testing.steps.users

import api_testing.models.User
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.io.InputStreamReader
import java.net.URISyntaxException

class CreateUsersStepdefs(
        private val userCreator: User,
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var user: JSONObject? = null

    // Scenario: The user registers in the platform

    @Given("^The user fills in his info for the platform$")
    fun userFillsInHisInfoForThePlatform() {

        this.user = this.userCreator.create()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.user.toString()
    }

    @Then("^The user should be registered$")
    @Throws(IOException::class)
    fun userShouldBeRegistered() {

        Assert.assertEquals(201, this.request.response!!.statusLine.statusCode.toLong())

        val registeredUser = JSONObject(JSONTokener(this.request.response!!.entity.content))

        this.user?.put("id", registeredUser.get("id"))
        JSONAssert.assertEquals(this.user, registeredUser, false)
    }

    // Scenario: The user registers with a blank username field

    @Given("^The user fills in his info for the platform with a blank \"([^\"]*)\" field$")
    fun userLeavesFieldBlank(field: String) {

        this.user = this.userCreator.createWithBlankField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.user.toString()
    }

    // Scenario: The user registers with a missing username field

    @Given("^The user fills in his info for the platform without a \"([^\"]*)\" field$")
    fun userLeavesOutField(field: String) {

        this.user = this.userCreator.createWithoutField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.user.toString()
    }

    // Scenario: The user registers with an extra field

    @Given("^The user fills in his info for the platform with an extra \"([^\"]*)\" field$")
    fun userAddsField(field: String) {

        this.user = this.userCreator.createWithExtraField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.user.toString()
    }

    // Scenario: The user registers with a malformed JSON

    @When("^The user sends his info with a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userSendsAMalformedJson() {

        this.user = this.userCreator.create()
        val jsonUser: String = this.user.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = jsonUser.substring(0, jsonUser.length - 2)
    }

    // Scenario: The user registers with an empty body

    @When("^The user sends no info$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userSendsNoInfo() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
    }

    @Given("^The user wants to register with an empty JSON$")
    fun userWantsToRegisterWithEmptyJson() {

        this.user = JSONObject()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.user.toString()
    }
}
