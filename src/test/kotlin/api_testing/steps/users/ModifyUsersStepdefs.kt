package api_testing.steps.users

import api_testing.models.User
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import com.github.javafaker.Faker
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.io.InputStreamReader
import java.net.URISyntaxException

class ModifyUsersStepdefs(
        private val userCreator: User,
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var user: JSONObject? = null

    // Scenario: The user changes some of his info

    @Given("^The user #([^\"]*) wants to change their \"([^\"]*)\"$")
    fun userWantsToChangeField(userId: String, field: String) {

        this.user = this.userCreator.updateField(field)
        this.endpoint.addToPath(userId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.user.toString()
    }

    @Then("^The user should be successful in changing his \"([^\"]*)\"$")
    @Throws(IOException::class)
    fun userShouldBeSuccessfulInFieldChange(field: String) {

        val modifiedUser = JSONObject(JSONTokener(this.request.response!!.entity.content))
        Assert.assertEquals(modifiedUser.get(field), this.user!!.get(field))
    }

    // Scenario: The user changes all of his info

    @Given("^The user #([^\"]*) wants to update all of his info$")
    fun userWantsToUpdateAllInfo(userId: String) {

        this.user = this.userCreator.create()
        this.endpoint.addToPath(userId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.user.toString()
    }

    @Then("^The user should be successful in changing his info$")
    @Throws(IOException::class)
    fun userShouldBeSuccessfulInChange() {

        val updatedUser = JSONObject(JSONTokener(this.request.response!!.entity.content))

        this.user!!.put("id", updatedUser.get("id"))
        JSONAssert.assertEquals(this.user, updatedUser, false)
    }

    // Scenario: A user tries to change all of his info but leaves the email field blank

    @Given("^The user #(\\d+) wants to update all of his info with a blank \"([^\"]*)\" field$")
    fun userLeavesFieldBlank(userId: Int, field: String) {

        this.user = this.userCreator.createWithBlankField(field)
        this.endpoint.addToPath(userId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.user.toString()
    }

    // Scenario: A user tries to change part of his info and leaves it blank

    @Given("^The user #(\\d+) wants to change their \"([^\"]*)\" to blank$")
    fun userChangesFieldToBlank(userId: Int, field: String) {

        this.user = JSONObject()
        this.user!!.put(field, "")
        this.endpoint.addToPath(userId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.user.toString()
    }

    // Scenario: A user tries to change all his info but leaves out the email field

    @Given("^The user #(\\d+) wants to update all of his info without a \"([^\"]*)\" field$")
    fun userLeavesOutField(userId: Int, field: String) {

        this.user = this.userCreator.createWithoutField(field)
        this.endpoint.addToPath(userId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.user.toString()
    }

    // Scenario: A user trues to change his email but leaves out that field

    @Given("^The user #(\\d+) wants to change their \"([^\"]*)\" without that field$")
    fun userUpdatesWithoutField(userId: Int, field: String) {

        this.user = JSONObject()
        this.endpoint.addToPath(userId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.user.toString()
    }

    @Given("^The user #(\\d+) wants to update all of his info with an extra \"([^\"]*)\" field$")
    fun userUpdatesAllWithExtraField(userId: Int, field: String) {

        this.user = this.userCreator.createWithExtraField(field)
        this.endpoint.addToPath(userId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.user.toString()
    }

    // Scenario: The user changes all of his info but sends a malformed JSON

    @When("^The user tries to change all of his info but sends a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userSendsMalformedJsonWithAllOfHisInfo() {

        val jsonUser: String = this.user!!.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = jsonUser.substring(0, jsonUser.length - 2)
    }

    // Scenario: The user changes some of his info but sends a malformed JSON

    @When("^The user tries to change part of his info but sends a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userSendsMalformedJSONWithPartialInfo() {

        val jsonUser: String = this.user!!.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = jsonUser.substring(0, jsonUser.length - 2)
    }

    // Scenario: The user wants to change all of his info but sends an empty body

    @When("^The user tries to change all of his info with an empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToChangeAllInfoWithEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
    }

    // Scenario: The user wants to change some of his info but sends an empty body

    @When("^The user tries to change part of his info with an empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToChangePartOfHisInfoWithEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
    }
}
