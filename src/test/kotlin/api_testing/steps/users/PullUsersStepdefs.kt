package api_testing.steps.users

import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

import java.io.IOException
import java.net.URISyntaxException
import java.util.HashMap

class PullUsersStepdefs(
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    // Scenario: The user retrieves information about an account.

    @When("^The user retrieves info about the account with username \"([^\"]*)\"$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userRetrievesInfoAboutAccount(username: String) {

        val urlParameters = HashMap<String, String>()
        urlParameters["username"] = username

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
        this.request.urlParameters = urlParameters
    }

    @Then("^The user should receive all the info of the account with username \"([^\"]*)\"$")
    @Throws(IOException::class)
    fun userShouldReceiveInfoOfAccount(username: String) {

        val user: JSONObject = JSONArray(JSONTokener(this.request.response!!.entity.content)).getJSONObject(0)
        Assert.assertEquals(username, user.get("username"))
    }

    // Scenario: The user retrieves information about all accounts.

    @When("^The user retrieves info about all accounts$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userRetrievesInfoAboutAllAccounts() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
    }

    @Then("^The user should receive all the info of all the accounts$")
    @Throws(IOException::class)
    fun userShouldReceiveInfoOfAllAccounts() {

        val users = JSONArray(JSONTokener(this.request.response!!.entity.content))
        Assert.assertNotNull(users)
    }

    @Then("^The user should receive an empty list$")
    @Throws(IOException::class)
    fun userShouldReceiveAnEmptyList() {

        val users = JSONArray(JSONTokener(this.request.response!!.entity.content))
        Assert.assertEquals(0, users.length().toLong())
    }
}
