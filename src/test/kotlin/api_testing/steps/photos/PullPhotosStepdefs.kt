package api_testing.steps.photos

import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

import java.io.IOException
import java.net.URISyntaxException
import java.util.HashMap

class PullPhotosStepdefs(
        private val endpoint: EndpointAccess,
        private val urlParameters: HashMap<String, String>,
        private val request: HttpRequest
) {


    @Given("The user wants to get the #([^\"]*) photo")
    fun userWantsToGetPhoto(photoId: String) {

        this.endpoint.addToPath(photoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
    }

    @Then("The user should get the #(\\d+) photo")
    @Throws(IOException::class)
    fun userShouldGetPhoto(photoId: Int) {

        val photo = JSONObject(JSONTokener(this.request.response!!.entity.content))

        Assert.assertEquals(photoId, photo.get("id"))
    }

    @Given("The user wants to get all the photos from the #([^\"]*) album")
    fun userShouldGetAllPhotosFromAlbum(albumId: String) {

        this.urlParameters["albumId"] = albumId

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
        this.request.urlParameters = this.urlParameters
    }

    @Then("The user should get all the photos from the #(\\d+) album")
    @Throws(IOException::class)
    fun userShouldGetPhotosFromAlbum(albumId: Int) {

        val album = JSONArray(JSONTokener(this.request.response!!.entity.content))

        Assert.assertTrue(album.length() > 0)
        Assert.assertEquals(albumId, album.getJSONObject(0).get("albumId"))
    }
}
