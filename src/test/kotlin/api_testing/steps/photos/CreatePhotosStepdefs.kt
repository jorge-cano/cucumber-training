package api_testing.steps.photos

import api_testing.models.Photo
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.net.URISyntaxException

class CreatePhotosStepdefs(
        private val endpoint: EndpointAccess,
        private val photoCreator: Photo,
        private val request: HttpRequest
) {

    private var photo: JSONObject? = null

    @Given("The user wants to upload a photo")
    fun userWantsToUploadPhoto() {

        this.photo = this.photoCreator.create()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.photo.toString()
    }

    @Then("The photo should be created")
    @Throws(IOException::class)
    fun photoShouldBeCreated() {

        val createdPhoto = JSONObject(JSONTokener(this.request.response!!.entity.content))
        this.photo!!.put("id", createdPhoto.get("id"))

        JSONAssert.assertEquals(this.photo, createdPhoto, false)
    }

    @Given("The user wants to upload a photo with a blank \"([^\"]*)\" field")
    fun userWantsToUploadPhotoWithBlankField(field: String) {

        this.photo = this.photoCreator.createWithBlankField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.photo.toString()
    }

    @Given("The user wants to upload a photo with a missing \"([^\"]*)\" field")
    fun userWantsToUploadPhotoWithoutField(field: String) {

        this.photo = this.photoCreator.createWithoutField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.photo.toString()
    }

    @Given("The user wants to upload a photo with an extra \"([^\"]*)\" field")
    fun userWantsToUploadPhotoWithExtraField(field: String) {

        this.photo = this.photoCreator.createWithExtraField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.photo.toString()
    }

    @When("The user tries to upload a photo with an invalid JSON")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToUploadPhotoWithInvalidJson() {

        val jsonPhoto: String = this.photo!!.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = jsonPhoto.substring(0, jsonPhoto.length - 2)
    }

    @When("The user wants to upload an empty JSON")
    fun userWantsToUploadEmptyJson() {

        this.photo = JSONObject()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.photo.toString()
    }

    @When("The user tries to send an empty body")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToSendEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
    }
}
