package api_testing.steps.photos

import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.net.URISyntaxException

class ModifyPhotosStepdefs(
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var photo: JSONObject? = null

    @Given("^The user wants to change the \"([^\"]*)\" to \"([^\"]*)\" in photo #([^\"]*)$")
    fun userWantsToChangePhoto(field: String, newValue: String, photoId: String) {

        this.endpoint.addToPath(photoId)
        this.photo = JSONObject()
        this.photo?.put(field, newValue)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.photo.toString()
    }

    @Then("The photo should be updated")
    @Throws(IOException::class)
    fun photoShouldBeUpdated() {

        val updatedPhoto = JSONObject(JSONTokener(this.request.response!!.entity.content))

        for (key in this.photo!!.keySet()) {

            Assert.assertEquals(this.photo?.get(key), updatedPhoto.get(key))
        }
    }

    @When("The user tries to update the photo with a malformed JSON")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToUpdateThePhotoWithMalformedJson() {

        val jsonPhoto: String = this.photo.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = jsonPhoto.substring(0, jsonPhoto.length - 2)
    }

    @Given("The user wants to update photo #([^\"]*) with an empty JSON")
    fun userWantsToUpdatePhotoWithEmptyJson(photoId: String) {

        this.photo = JSONObject()
        this.endpoint.addToPath(photoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.photo.toString()
    }

    @When("The user tries to update photo #([^\"]*) with an empty body")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToUpdateThePhotoWithEmptyBody(photoId: String) {

        this.endpoint.addToPath(photoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
    }
}
