package api_testing.steps.kafka

import api_testing.models.User
import api_testing.utils.JSONSimilar
import api_testing.utils.KafkaRemapConsumer
import api_testing.utils.KafkaRemapProducer
import com.github.javafaker.Faker
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.kafka.common.protocol.types.Field
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareResult

class KafkaRemapStepdefs(
        private val personCreator: User
) {

    private var person: JSONObject? = null
    private var garbageData: String? = null

    @Given("^I have an old style person record with a \"([^\"]*)\" gender$")
    fun haveAnOldStylePersonRecordWithGender(gender: String){

        this.person = this.personCreator.createWithGender(gender)
    }

    @When("^I send the record to the \"([^\"]*)\" topic$")
    fun sendTheRecord(topic: String){

        KafkaRemapProducer(topic).sendToTopic(this.person.toString())
    }

    @Then("^I should get the record with a \"([^\"]*)\" gender$")
    fun shouldGetRecordWithGender(gender: String){

        val convertedPerson = JSONObject(JSONTokener(
                KafkaRemapConsumer("streams-remap-output").consumeTopicRecord()
        ))

        Assert.assertEquals(gender, convertedPerson["gender"])

        this.person?.remove("gender")
        convertedPerson.remove("gender")

        JSONAssert.assertEquals(this.person!!, convertedPerson, false)
    }

    @When("^I have an old style person record with no gender field$")
    fun haveAnOldStyleRecordWithoutGender(){

        this.person = this.personCreator.create()
    }

    @Then("^I should see the same record in the \"([^\"]*)\" topic$")
    fun shouldSeeTheSameRecordInTopic(topic: String){

        val topicRecord = JSONObject(JSONTokener(
                KafkaRemapConsumer(topic).consumeTopicRecord()
        ))

        JSONAssert.assertEquals(this.person, topicRecord, false)
    }

    @When("^I send garbage data$")
    fun sendGarbageData(){

        this.garbageData = Faker().rickAndMorty().quote()
        KafkaRemapProducer("streams-remap-input").sendToTopic(this.garbageData!!)
    }

    @Then("^I should see that same data in the \"([^\"]*)\" topic$")
    fun shouldSeeTheSameDataInTopic(topic: String){

        Assert.assertEquals(
                this.garbageData,
                KafkaRemapConsumer(topic).consumeTopicRecord()
        )
    }

    @Given("^I have this old style record:$")
    fun haveThisOldStyleRecord(json: String){

        this.person = JSONObject(JSONTokener(json))
    }

    @Then("^I should see the following record in the \"([^\"]*)\" topic:$")
    fun shouldSeeTheFollowingRecordInTopic(topic: String, expectedJson: String){

        JSONAssert.assertEquals(
                expectedJson,
                KafkaRemapConsumer(topic).consumeTopicRecord(),
                false
        )
    }

    @Then("^I should see a record similar to this one in the \"([^\"]*)\" topic:$")
    fun shouldSeeARecordSimilarToThisInTopic(topic: String, expectedJson: String){

        val expectedRecord = JSONObject(JSONTokener(expectedJson))
        val actualRecord = JSONObject(JSONTokener(KafkaRemapConsumer(topic).consumeTopicRecord()))

        JSONSimilar.assertSimilar(expectedRecord, actualRecord)
    }

}