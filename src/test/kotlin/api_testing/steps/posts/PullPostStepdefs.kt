package api_testing.steps.posts

import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder

import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

import java.io.IOException
import java.net.URISyntaxException
import java.util.HashMap


class PullPostStepdefs(
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    // Scenario Outline: User pulls the n'th post
    //--------------------------------------------------------------------------------------------
    @When("^The user asks for the #([^\"]*) post$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userAsksForPost(postNumber: String) {

        this.endpoint.addToPath(postNumber)
        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
    }

    @Then("^The user should be served the #(\\d+) post$")
    @Throws(IOException::class)
    fun userShouldBeServedPostN(expectedPostNumber: Int) {

        val post = JSONObject(JSONTokener(this.request.response!!.entity.content))
        Assert.assertEquals(expectedPostNumber, post.get("id"))
    }

    // Scenario: User pulls a list of all the posts
    //--------------------------------------------------------------------------------------------
    @When("^The user asks for all the posts$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userAsksForAllPosts() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
    }

    @Then("^The user should be served a list of posts$")
    @Throws(IOException::class)
    fun userShouldBeServedListOfPosts() {

        val posts = JSONArray(JSONTokener(this.request.response!!.entity.content))
        Assert.assertNotNull(posts)
    }

    // Scenario: User pulls a list of all the posts of another user
    //--------------------------------------------------------------------------------------------

    @Then("^The user should be served all the posts of user #(\\d+)$")
    @Throws(IOException::class)
    fun userShouldBeServedThePostsFromUser(userId: Int) {

        val posts = JSONArray(JSONTokener(this.request.response!!.entity.content))

        Assert.assertNotNull(posts)
        if (posts.length() > 0) {

            val post: JSONObject = posts.getJSONObject(0)
            Assert.assertEquals(userId, post.get("userId"))
        }
    }

    // Scenario: User tries to pull posts of a user that does not exist
    //--------------------------------------------------------------------------------------------
    @Then("^The user should get an empty list of posts$")
    @Throws(IOException::class)
    fun userShouldGetEmptyListOfPosts() {

        val posts = JSONArray(JSONTokener(this.request.response!!.entity.content))

        Assert.assertNotNull(posts)
        Assert.assertEquals(0, posts.length().toLong())
    }

    // Scenario: User tries to pull posts of a user with an invalid value
    //--------------------------------------------------------------------------------------------
    @When("^The user asks for all of the posts of user #\"([^\"]*)\"$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userAsksForAllThePostsOfUser(userId: String) {

        val urlParameters = HashMap<String, String>()
        urlParameters["userId"] = userId

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
        this.request.urlParameters = urlParameters
    }
}
