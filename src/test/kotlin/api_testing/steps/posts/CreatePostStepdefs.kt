package api_testing.steps.posts

import api_testing.models.Post
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import com.github.javafaker.Faker
import cucumber.api.java.en.But
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.io.InputStreamReader
import java.net.URISyntaxException

class CreatePostStepdefs(
        private val postCreator: Post,
        private val endpoint: EndpointAccess,
        private var request: HttpRequest
) {

    private var post = JSONObject()

    // Scenario: User wants to post something
    //--------------------------------------------------------------------------------------------
    @Given("^That the user wants to post something new$")
    fun userWantsToPostNew() {

        this.post = postCreator.create()
        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.post.toString()
    }

    @Then("^A new post should be created successfully$")
    @Throws(IOException::class)
    fun postCreationShouldBeSuccessful() {

        val post = JSONObject(JSONTokener(this.request.response!!.entity.content))

        this.post.put("id", post.get("id"))
        JSONAssert.assertEquals(this.post, post, false)
    }

    // Scenario: User wants to post with a blank title field
    //--------------------------------------------------------------------------------------------
    @Given("^That the user wants to post something new with a blank ([^\"]*) field$")
    fun theUserLeavesFieldBlank(field: String) {

        this.post = this.postCreator.createWithBlankField(field)
        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.post.toString()
    }

    // Scenario User wants to post something without a title field
    //--------------------------------------------------------------------------------------------
    @Given("^That the user wants to post something new without a ([^\"]*) field$")
    fun userLeavesOutField(field: String) {

        this.post = this.postCreator.createWithMissingField(field)
        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.post.toString()
    }

    // Scenario: User wants to post something with an extra field
    //--------------------------------------------------------------------------------------------
    @Given("^That the user wants to post something new with an extra ([^\"]*) field$")
    fun userAddsExtraField(field: String) {

        this.post = this.postCreator.createWithExtraField(field)
        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.post.toString()
    }

    // Scenario: User wants to post something but has a malformed JSON
    //--------------------------------------------------------------------------------------------
    @When("^The user tries to post something with a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToPostWithMalformedJSON() {

        this.post = this.postCreator.create()
        val jsonString: String = this.post.toString()
        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = jsonString.substring(0, jsonString.length - 2)
    }

    @When("^The user tries to post something with an empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToPostWithEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
    }

    @Given("^That the user wants to post something with an empty JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToPostWithEmptyJson() {

        this.post = JSONObject()
        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.post.toString()
    }
}
