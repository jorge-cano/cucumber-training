package api_testing.steps.posts


import api_testing.models.Post
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse

import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

import java.io.IOException
import java.net.URISyntaxException


class ModifyPostsStepdefs(
        private val postCreator: Post,
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var post: JSONObject? = null

    // Scenario: User wants to edit a post's title
    //--------------------------------------------------------------------------------------------

    @Given("^That the user wants to change \"([^\"]*)\" of post #([^\"]*)$")
    fun userWantsToChangePost(fieldToAmend: String, postNumber: String) {

        this.post = this.postCreator.updateField(fieldToAmend)
        this.endpoint.addToPath(postNumber)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.post.toString()
    }


    @Then("^The changes to the post should be successful$")
    @Throws(IOException::class)
    fun changesShouldBeSuccessful() {

        val post = JSONObject(JSONTokener(this.request.response!!.entity.content))
        Assert.assertNotNull(post)
    }

    // Scenario: User wants to edit a post but sends an malformed JSON
    //--------------------------------------------------------------------------------------------
    @When("^The user tries to edit the post with a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToEditPostWithMalformedJson() {

        val jsonString: String = this.post!!.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = jsonString.substring(0, jsonString.length - 2)
    }

    @When("^The user tries to edit post #(\\d+) with an empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToEditPostWithEmptyBody(postId: Int) {

        this.endpoint.addToPath(postId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
    }

    @Given("^That the user wants to edit post #(\\d+) with an empty JSON$")
    fun userTriesToEditPostWithEmptyJson(postId: Int) {

        this.post = JSONObject()
        this.endpoint.addToPath(postId.toString())

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.post.toString()
    }

    @Given("^That the user wants to change \"([^\"]*)\" of post #\"([^\"]*)\" to blank$")
    fun userWantsToChangeFieldToBlank(field: String, postId: String) {

        this.post = this.postCreator.updateFieldToBlank(field)
        this.endpoint.addToPath(postId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PATCH"
        this.request.payload = this.post.toString()
    }
}
