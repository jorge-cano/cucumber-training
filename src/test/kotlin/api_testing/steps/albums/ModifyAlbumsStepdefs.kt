package api_testing.steps.albums

import api_testing.models.Album
import api_testing.models.Comment
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

class ModifyAlbumsStepdefs(
        private val albumCreator: Album,
        private val request: HttpRequest,
        private val endpoint: EndpointAccess
) {

    private var album: JSONObject? = null

    @Given("^The user wants to change album #\"([^\"]*)\" with the following values:$")
    fun userWantsToChangeAlbum(albumId: String, values: Map<String, String>) {

        this.album = this.albumCreator.createWithValues(values)
        this.endpoint.addToPath(albumId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.album.toString()
    }

    @Then("^The user should be successful in changing the album info$")
    fun userShouldBeSuccessful() {

        val modifiedAlbum = JSONObject(JSONTokener(this.request.response?.entity?.content))

        for (key in this.album!!.keys()) {

            Assert.assertEquals(this.album!![key], modifiedAlbum[key])
        }
    }

    @Given("^The user wants to change album #\"([^\"]*)\" with an empty JSON$")
    fun userWantsToChangeAlbumWithEmptyJson(albumId: String) {

        this.album = JSONObject()
        this.endpoint.addToPath(albumId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.album.toString()
    }

    @When("^The user tries to change the album info with a malformed JSON$")
    fun userTriesToChangeTheAlbumWithMalformedJson() {

        val jsonAlbum: String = this.album.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = jsonAlbum.substring(0, jsonAlbum.length - 2)
    }

    @When("^The user tries to change the album info with an empty body$")
    fun userTriesToChangeTheAlbumInfoWithEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
    }
}