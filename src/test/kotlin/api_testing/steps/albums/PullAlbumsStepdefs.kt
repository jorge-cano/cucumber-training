package api_testing.steps.albums

import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert
import java.util.HashMap

class PullAlbumsStepdefs(
        private val endpoint: EndpointAccess,
        private val urlParameters: HashMap<String, String>,
        private val request: HttpRequest
) {

    @When("^The user pulls the photo albums$")
    fun userPullsThePhotoAlbums() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
    }

    @Then("^The user should get all the photo albums$")
    fun userShouldGetAllThePhotoAlbums(){

        val albums = JSONArray(JSONTokener(this.request.response?.entity?.content))
        Assert.assertTrue(albums.length() > 0)
    }

    @Given("^The user wants the albums of user #\"([^\"]*)\"$")
    fun userWantsAlbumsFromUser(userId: String){

        this.urlParameters["userId"] = userId

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
        this.request.urlParameters = urlParameters
    }

    @Then("^The user should get all the albums of user #\"(\\d+)\"$")
    fun userShouldGetAllAlbumsFromUser(userId: Int){

        val albums = JSONArray(JSONTokener(this.request.response?.entity?.content))

        if(albums.length() > 0){
            for(album in albums){

                Assert.assertEquals(userId, (album as JSONObject).get("userId"))
            }
        }
    }

    @Given("^The user wants the photo album #\"([^\"]*)\"$")
    fun userWantsPhotoAlbum(albumId: String){

        this.endpoint.addToPath(albumId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
    }

    @Then("^The user should get the photo album #\"(\\d+)\"$")
    fun userShouldGetPhotoAlbum(albumId: Int){

        val album = JSONObject(JSONTokener(this.request.response?.entity?.content))

        Assert.assertEquals(albumId, album.get("id"))
    }
}