package api_testing.steps.albums

import api_testing.models.Album
import api_testing.models.Comment
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.skyscreamer.jsonassert.JSONAssert

class CreateAlbumsStepdefs(
        private val albumCreator: Album,
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var album: JSONObject? = null

    @Given("^The user wants to create a new photo album$")
    fun userWantsToCreateANewPhotoAlbum(){

        this.album = this.albumCreator.create()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.album.toString()
    }

    @Then("^The user should successfully create a new photo album$")
    fun userShouldSuccessfullyCreatePhotoAlbum(){

        val createdAlbum = JSONObject(JSONTokener(this.request.response?.entity?.content))
        this.album?.put("id", createdAlbum["id"])

        JSONAssert.assertEquals(this.album, createdAlbum, false)
    }

    @Given("^The user tries to create a new photo album$")
    fun theUserTriesToCreateANewAlbum(){

        this.album = JSONObject()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.album.toString()
    }

    @Given("^The user wants to create a new photo album with a blank \"([^\"]*)\"$")
    fun userWantsToCreateNewAlbumWithBlankField(field: String){

        this.album = this.albumCreator.createWithBlankField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.album.toString()
    }

    @Given("^The user wants to create a new photo album without a \"([^\"]*)\"$")
    fun userWantsToCreateNewAlbumWithoutField(field: String){

        this.album = this.albumCreator.createWithoutField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.album.toString()
    }

    @Given("^The user wants to create a new photo album with an extra \"([^\"]*)\"$")
    fun userWantsToCreateNewAlbumWithExtraField(field: String){

        this.album = this.albumCreator.createWithExtraField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.album.toString()
    }

    @When("^The user tries to create a new photo album with a malformed JSON$")
    fun userTriesToCreateNewAlbumWithMalformedJson(){

        this.album = this.albumCreator.create()
        val jsonAlbum: String = this.album.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = jsonAlbum.substring(0, jsonAlbum.length - 2)
    }

    @When("^The user tries to create a photo album without a body$")
    fun userTriesToCreateNewAlbumWithoutBody(){

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
    }
}