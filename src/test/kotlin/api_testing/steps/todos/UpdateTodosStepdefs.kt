package api_testing.steps.todos

import api_testing.models.Todo
import api_testing.models.User
import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import com.github.javafaker.Faker
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

import java.io.IOException
import java.net.URISyntaxException

class UpdateTodosStepdefs(
        private val todoCreator: Todo,
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var todo: JSONObject? = null

    @Given("^The user wants to mark todo #([^\"]*) as done$")
    fun markTodoAsDone(todoId: String) {

        this.todo = this.todoCreator.markAs(true)
        this.endpoint.addToPath(todoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.todo.toString()
    }

    @Then("^The update should be successful$")
    @Throws(IOException::class)
    fun updateShouldBeSuccessful() {

        val updatedTodo = JSONObject(JSONTokener(this.request.response!!.entity.content))

        for (key in this.todo!!.keySet()) {

            Assert.assertEquals(this.todo!!.get(key), updatedTodo.get(key))
        }
    }

    @Given("^The user wants to change the \"([^\"]*)\" of todo #([^\"]*)$")
    fun updateTodoField(field: String, todoId: String) {

        this.todo = this.todoCreator.updateField(field)
        this.endpoint.addToPath(todoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.todo.toString()
    }

    @Given("^The user wants mark todo #([^\"]*) as done with an invalid value$")
    fun markTodoAsDoneWith(todoId: String) {

        this.todo = this.todoCreator.updateField("completed")
        this.endpoint.addToPath(todoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.todo.toString()
    }

    @Given("^The user wants to update todo #([^\"]*) with an empty JSON$")
    fun updateTodoWithEmpty(todoId: String) {

        this.todo = JSONObject()
        this.endpoint.addToPath(todoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.todo.toString()
    }

    @Given("^The user wants to change the \"([^\"]*)\" of todo #\"([^\"]*)\" to blank$")
    fun updateTodoFieldToBlank(field: String, todoId: String) {

        this.todo = this.todoCreator.updateFieldToBlank(field)
        this.endpoint.addToPath(todoId)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = this.todo.toString()
    }

    @When("^The user tries to update the todo with an empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToupdateTodoWithEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
    }

    @When("^The user tries to update the todo with a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userTriesToupdateTodoWithMalformedJson() {

        val jsonTodoUpdate: String = this.todo!!.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "PUT"
        this.request.payload = jsonTodoUpdate.substring(0, jsonTodoUpdate.length - 2)
    }
}
