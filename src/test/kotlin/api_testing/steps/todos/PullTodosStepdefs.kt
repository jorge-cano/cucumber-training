package api_testing.steps.todos

import api_testing.utils.EndpointAccess
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import api_testing.utils.ResponseCodeValidator
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.http.HttpResponse
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import org.junit.Assert

import java.io.IOException
import java.net.URISyntaxException
import java.util.HashMap

class PullTodosStepdefs(
        private val endpoint: EndpointAccess,
        private val urlParameters: HashMap<String, String>,
        private val request: HttpRequest
) {

    @Given("^The user #([^\"]*) wants to get his to-dos$")
    fun userWantsHisTodos(userId: String) {

        this.urlParameters["userId"] = userId

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "GET"
        this.request.urlParameters = this.urlParameters
    }

    @Then("^The user #(\\d+) should get a list with their to-dos$")
    @Throws(IOException::class)
    fun userShouldGetTodos(userId: Int) {

        val todos = JSONArray(JSONTokener(this.request.response!!.entity.content))

        if (todos.length() > 0) {

            Assert.assertEquals(userId.toLong(), (todos.get(1) as JSONObject).getInt("userId").toLong())
        }
    }

    @Then("^The user will get an error code (\\d+)$")
    fun userWillGetError(errorCode: Int) {

        Assert.assertEquals(errorCode.toLong(), this.request.response!!.statusLine.statusCode.toLong())
    }

    @Given("^A user without id tries to get his to-dos$")
    fun userWithNoIdWantsTodos() {

        this.urlParameters["userId"] = ""

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.urlParameters = this.urlParameters
    }

}
