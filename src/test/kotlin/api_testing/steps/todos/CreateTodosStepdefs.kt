package api_testing.steps.todos

import api_testing.utils.EndpointAccess
import api_testing.models.Todo
import api_testing.utils.HttpRequest
import api_testing.utils.RequestSetupHandler
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.json.JSONObject
import org.json.JSONTokener
import org.skyscreamer.jsonassert.JSONAssert

import java.io.IOException
import java.net.URISyntaxException

class CreateTodosStepdefs(
        private val todoCreator: Todo,
        private val endpoint: EndpointAccess,
        private val request: HttpRequest
) {

    private var todo: JSONObject? = null

    @Given("^The user has a new to-do$")
    fun userHasNewTodo() {

        this.todo = this.todoCreator.create()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.todo.toString()
    }

    @Then("^The new todo should be created$")
    @Throws(IOException::class)
    fun newTodoShouldBeCreated() {

        val createdTodo = JSONObject(JSONTokener(this.request.response!!.entity.content))

        this.todo!!.put("id", createdTodo.get("id"))
        JSONAssert.assertEquals(this.todo, createdTodo, false)
    }

    @Given("^The user wants to create a todo with a blank ([^\"]*)$")
    fun userWantsToCreateTodoWithBlank(field: String) {

        this.todo = this.todoCreator.createWithBlankField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.todo.toString()
    }

    @Given("^The user wants to create a todo leaving out the ([^\"]*)$")
    fun userWantsToCreateTodoWithout(field: String) {

        this.todo = this.todoCreator.createWithoutField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.todo.toString()
    }

    @Given("^The user wants to create a todo with an extra \"([^\"]*)\" field$")
    fun userWantsToCreateTodoWithExtraField(field: String) {

        this.todo = this.todoCreator.createWithExtraField(field)

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.todo.toString()
    }

    @Given("^The user wants to create a todo sending an empty JSON$")
    fun userWantsToCreateTodoWithEmptyJson() {

        this.todo = JSONObject()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = this.todo.toString()
    }

    @When("^The user tries to create a new todo with a malformed JSON$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userSendsMalformedJSON() {

        this.todo = this.todoCreator.create()
        val todoJson: String = this.todo!!.toString()

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
        this.request.payload = todoJson.substring(0, todoJson.length - 2)
    }

    @When("^The user tries to create a new todo with an empty body$")
    @Throws(IOException::class, URISyntaxException::class)
    fun userSendsEmptyBody() {

        this.request.url = this.endpoint.targetURL
        this.request.requestMethod = "POST"
    }
}
