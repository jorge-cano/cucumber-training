package api_testing

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(Cucumber::class)
@CucumberOptions(
        plugin = arrayOf("pretty", "io.cucumber.pro.JsonReporter:all"),
        tags = arrayOf("@RunThis"),
        features = arrayOf("src/test/resources"))
class RunSelected
